package com.example.androidskillsdevelopment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button number;


    public void onClickSelected(View view){

        EditText myText = (EditText) findViewById(R.id.myText);
        TextView textInput = (TextView) findViewById(R.id.textInput);
        Log.i("SAM", myText.getText().toString());
         textInput.setText(myText.getText().toString());

//        number2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:"+number2));
//                startActivity(intent);
//            }
//        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        number= findViewById(R.id.number);

        number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:"+"samiasalhia789@gmail.com"));
                intent.putExtra(Intent.EXTRA_SUBJECT,"HELLO");
                intent.putExtra(Intent.EXTRA_TEXT,"HI");
                startActivity(intent);
            }
        });


    }
}